-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here




display.setStatusBar( display.HiddenStatusBar )

local fisica = require("physics")
fisica.start()
fisica.setGravity(0, 0)

--fisica.setDrawMode("hybrid")
system.activate("multitouch")

local background = display.newImage("openphotonet_green_sea.jpg");

-------------------------
-- Primeras cadenas
--------------------------
--local cadenaADN1 = display.newImage("cadena.png")
--cadenaADN:setReferencePoint(display.BottomLeftReferencePoint)
--cadenaADN1.x=100
--cadenaADN1.y=800
--cadenaADN1.speed=1

--local cadenaADN2 = display.newImage("cadena.png")
--cadenaADN:setReferencePoint(display.BottomLeftReferencePoint)
--cadenaADN2.x=100
--cadenaADN2.y=1350
--cadenaADN2.speed=1

--- Funciones de movimiento de cadenas
--function moverCadenas(self, event)
--	if self.y < -320 then
--		self.y = 750
--	else
--		self.y = self.y - self.speed
--	end
--end

--cadenaADN1.enterFrame = moverCadenas
--Runtime:addEventListener("enterFrame",cadenaADN1)
--cadenaADN2.enterFrame = moverCadenas
--Runtime:addEventListener("enterFrame",cadenaADN2)
-------------------------------------------------------------

--------------------------------
----- Formar la cadena aleatoria
--------------------------------
local function formarCadena()
	local i = math.random(4)
	print(i)
	local figura
	if (i==1) then
			figura = display.newImageRect("cadena_adenina.png", 66,31)
			print("cadena_adenina")
		elseif(i==2) then
				figura = display.newImageRect("cadena_citosina.png", 66,31)
				print("cadena_citosina")
			elseif(i==3) then
					figura = display.newImageRect("cadena_guanina.png", 66,31)
					print("cadena_guanina")
				else
					figura = display.newImageRect("cadena_timina.png", 66,31)
					print("cadena_timina")
				end
--	figura.x = math.random(150,320)
	figura.x = 50
	figura.y = 500
	figura.speed = -1
--	figura.rotation = 10
	fisica.addBody(figura, {density = 1.0, friction = 0.5})
	
	figura:addEventListener("touch", moverFigura)
	
	figura.enterFrame = caerFiguras
	Runtime:addEventListener("enterFrame",figura)
end

timer.performWithDelay(1000,formarCadena, 50)

--------------------------------------
local leftWall = display.newRect(0, 0, 1, display.contentHeight)
fisica.addBody(leftWall, "static", {bounce = 0.1})
local rightWall = display.newRect(display.contentWidth, 0, 1, display.contentHeight)
fisica.addBody(rightWall, "static", {bounce = 0.1})
--local ceilling = display.newRect(0, 0, display.contentWidth,1)
--fisica.addBody(ceilling, "static", {bounce = 0.1})

--local pantalla1 = display.newImageRect( "fondo1_1.jpg", 320, 480 )
--pantalla1.x = 160
--pantalla1.y = 240

local pantalla2 = display.newImageRect( "fondo3_1.jpg", 320, 68 )
pantalla2.x = 160
pantalla2.y = 510
--fisica.addBody(pantalla2, "static", {bounce = 0.0, friction=10})

--Círculo
local circulo = display.newImageRect( "circulo.png", 60, 60)
circulo.id = "CirculoPrincipal"
circulo.x = 295
circulo.y = 502

local contador = 0 -- cuenta las figuras que salen de la pantalla
local marcador = display.newText( contador, 295, 500)
--
--function moverCirculo3(event)
--	local figura = event.target
--	figura:applyLinearImpulse(0, -0.2, event.x, event.y)
--end

function moverFigura(event)
	local figura = event.target
	if event.x>0 and event.x <=300 and event.y>0 and event.y<500 then
		figura.x = event.x
		figura.y = event.y
		if (event.x >0 and event.x <150) then
			figura.speed = - 1			
		end
	end
end --function moverFigura
	
function caerFiguras(self, event)
	pantalla2:toFront()
	circulo:toFront()
	marcador:toFront()
	if self.y == 480 then
		self.y = 900
		self.speed = 0
		contador = contador + 1
		print ("Contador: ", contador)
		marcador = display.newText( contador, 295, 500)
--		self:toBack()
	else
		self.y = self.y + self.speed
	end
end

local function muchasFiguras()
	local i = math.random(4)
	print(i)
	local figura
	if (i==1) then
			figura = display.newImageRect("adenina.png", 66,31)
			print("adenina")
		elseif(i==2) then
				figura = display.newImageRect("citosina.png", 66,31)
				print("citosina")
			elseif(i==3) then
					figura = display.newImageRect("guanina.png", 66,31)
					print("guanina")
				else
					figura = display.newImageRect("timina.png", 66,31)
					print("timina")
				end
	figura.x = math.random(150,320)
	figura.y = -30
	figura.speed = 2
--	figura.rotation = 10
	fisica.addBody(figura, {density = 1.0, friction = 0.5})
	
	figura:addEventListener("touch", moverFigura)
	
	figura.enterFrame = caerFiguras
	Runtime:addEventListener("enterFrame",figura)
end

timer.performWithDelay(1000,muchasFiguras, 50)

---------------------------
----- Esta función no hace nada.
----- La usé en un programa anterior para probar esas propiedades
---------------------------
function mostrarPantalla2(event)
	local botonMostrar = event.target
	botonOcultar:toFront()
	pantalla2:toFront()
	circulo:toFront()
	triangulo:toFront()
	cuadrado:toFront()
	imagenLinea:toFront()
	botonMostrar:toBack()
end
